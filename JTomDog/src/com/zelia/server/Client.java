package com.zelia.server;

import java.io.PrintWriter;

public class Client {
	private PrintWriter pw;
	private int idx;
	private String ip;
	
	public Client(String ip, PrintWriter pw, int idx){
		this.pw = pw;
		this.idx = idx;
		this.ip = ip;
	}
	
	public PrintWriter getPrintWriter(){
		return pw;
	}
	
	public int getIdx(){
		return idx;
	}
	
	public String toString(){
		return "Client " + idx + ": " + ip;
	}
}
