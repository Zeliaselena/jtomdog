package com.zelia.server;

import java.awt.BorderLayout;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

public class ServerView extends JFrame implements Observer {
	private JPanel pan;
	private JPanel panMsg;
	private JLabel nbClients;
	private JList<Client> clientsList;

	public ServerView(Server serv) {
		super("Server JTomDog (port " + serv.getPort() + ")");
		serv.addObserver(this);
		init();
	}

	private void init() {
		setSize(600, 400);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		initPan();
		setVisible(true);
	}

	private void initPan() {
		pan = new JPanel();
		pan.setLayout(new BorderLayout());

		Client[] nullList = new Client[1];
		nullList[0] = new Client("None", null, 0);
		clientsList = new JList<>(nullList);
		pan.add(clientsList, BorderLayout.EAST);

		panMsg = new JPanel();
		pan.add(panMsg, BorderLayout.CENTER);

		nbClients = new JLabel("Clients connected : " + "0");
		pan.add(nbClients, BorderLayout.SOUTH);

		add(pan);
	}

	@Override
	public void update(Observable o, Object arg) {
		Server serv = (Server) o;
		List<Client> lc = serv.getClients();
		
		System.out.println("AJOUT de : " + lc.toString());
		
		Client[] lcArray =  new Client[lc.size()];
		for(int i = 0; i < lcArray.length; ++i)
			lcArray[i] = lc.get(i);
		if(lcArray.length == 0){
			lcArray = new Client[1];
			lcArray[0] = new Client("None", null, 0);
		}
		clientsList.setListData(lcArray);
		nbClients.setText("Clients connected : " + serv.getNbClients());
	}
}
