package com.zelia.server;

import java.io.PrintWriter;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;

import javax.swing.JFrame;

public class Server extends Observable {
	private List<Client> clients = Collections.synchronizedList(new ArrayList<Client>());
	private int nbClients = 0;
	private static int port = 0;

	/**
	 * Wait for connections
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Server server = new Server();
		try {
			if (args.length <= 0)
				port = 18000;
			else
				port = Integer.parseInt(args[0]);

			new Commands(server);

			ServerSocket ss = new ServerSocket(port);
			printWelcome(port);

			new ServerView(server);
			
			while (true)
				new ServThread(ss.accept(), server);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Display welcome message
	 * 
	 * @param port
	 */
	static private void printWelcome(int port) {
		System.out.println("------");
		System.out.println("JTomDog : by Alexandre Beaudet");
		System.out.println("------");
		System.out.println("Work on port : " + port);
		System.out.println("------");
		System.out.println("Quit : type \"quit\"");
		System.out.println("Show clients amount : type \"total\"\n");
	}

	/**
	 * Send the message to all clients listening
	 * 
	 * @param message
	 * @param sLast
	 */
	synchronized public void sendAll(String message, String sLast) {
		PrintWriter out;
		for (int i = 0; i < clients.size(); ++i) {
			out = clients.get(i).getPrintWriter();
			if (out != null) {
				out.println(message + sLast);
				out.flush();
			}
		}
	}

	/**
	 * Delete the i client
	 * 
	 * @param i
	 */
	synchronized public void delClient(Client c) {
			clients.remove(c);
			--nbClients;
			setChanged();
			notifyObservers();
	}

	/**
	 * Add a new client
	 * 
	 * @param out
	 * @return Client's number
	 */
	synchronized public Client addClient(String ip, PrintWriter pw) {
		++nbClients;
		Client c = new Client(ip, pw, clients.size());
		clients.add(c);
		setChanged();
		notifyObservers();
		return c;
	}

	/**
	 * Return the amount of connected clients
	 * 
	 * @return
	 */
	synchronized public int getNbClients() {
		return nbClients;
	}

	public String getPort() {
		return Integer.toString(port);
	}

	public List<Client> getClients() {
		return clients;
	}

}
