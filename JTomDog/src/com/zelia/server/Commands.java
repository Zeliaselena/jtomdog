package com.zelia.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Commands implements Runnable {
	Server serv;
	BufferedReader in;
	String strCmd = "";
	Thread t;

	public Commands(Server serv) {
		this.serv = serv;
		in = new BufferedReader(new InputStreamReader(System.in));
		t = new Thread(this);
		t.start();
	}

	@Override
	public void run() {
		try {
			while ((strCmd = in.readLine()) != null) {
				if (strCmd.equalsIgnoreCase("quit"))
					System.exit(0);
				else if (strCmd.equalsIgnoreCase("total"))
					System.out.println(serv.getNbClients()
							+ " clients connected.");
				System.out.flush();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
