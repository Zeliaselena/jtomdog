package com.zelia.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ServThread implements Runnable {
	private Thread t;
	private Socket s;

	private PrintWriter out;
	private BufferedReader in;

	private Server serv;
	
	private Client c;

	public ServThread(Socket socket, Server serv) {
		this.serv = serv;
		s = socket;

		try {
			out = new PrintWriter(s.getOutputStream());
			in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			c = serv.addClient(s.getInetAddress().getHostAddress(), out);
		} catch (IOException e) {
			e.printStackTrace();
		}

		t = new Thread(this);
		t.start();
	}

	@Override
	public void run() {
		String msg = "";
		System.out.println("New connection : " + c);

		try {
			char c[] = new char[1];

			while (in.read(c, 0, 1) != -1) {
				if (c[0] != '\u0000' && c[0] != '\n' && c[0] != '\r')
					msg += c[0];
				else {
					if (c[0] == '\u0000')
						serv.sendAll(msg, "" + c[0]);
					else
						serv.sendAll(msg, "");
					msg = "";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				System.out.println(c + " disconnected.");
				serv.delClient(c);
				s.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
